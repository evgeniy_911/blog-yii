<?php

use yii\db\Migration;

/**
 * Class m200508_174841_nev_column
 */
class m200508_174841_nev_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('otvet', 'verno', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200508_174841_nev_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200508_174841_nev_column cannot be reverted.\n";

        return false;
    }
    */
}
