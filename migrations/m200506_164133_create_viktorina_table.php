<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%viktorina}}`.
 */
class m200506_164133_create_viktorina_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('vopros', [
            'id' => Schema::TYPE_PK,
            'vopros' => Schema::TYPE_STRING,
        ]);
        $this->createTable('otvet', [
            'id' => Schema::TYPE_PK,
            'vopros_id' => Schema::TYPE_INTEGER,
            'otvet' => Schema::TYPE_STRING,
            'kluch' => Schema::TYPE_STRING,
        ]);

        $this->createIndex('otvet_vopros_id', 'otvet', 'vopros_id');
        $this->addForeignKey('fk_otvet_vopros_id', 'otvet', 'vopros_id', 'vopros', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_otvet_vopros_id', 'otvet');
        $this->dropIndex('otvet_vopros_id', 'otvet');

        $this->dropTable('vopros');
        $this->dropTable('otvet');
    }
}
