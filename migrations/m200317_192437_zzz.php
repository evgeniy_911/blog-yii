<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m200317_192437_zzz
 */
class m200317_192437_zzz extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('zzz', [
            'id' => Schema::TYPE_PK,
            'category' => Schema::TYPE_STRING,
            'visits' => Schema::TYPE_INTEGER,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('zzz');
    }
}
