<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m200322_181906_init
 */
class m200322_181906_init extends Migration
{
    public function up()
    {
        $this->createTable('categories', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING,
            'url' => Schema::TYPE_STRING,
        ]);
        $this->createTable('products', [
            'id' => Schema::TYPE_PK,
            'category_id' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING,
            'url' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'status' => Schema::TYPE_INTEGER,
            'visits' => Schema::TYPE_INTEGER,
            'content' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_TIMESTAMP,
        ]);
        $this->createTable('comments', [
            'id' => Schema::TYPE_PK,
            'product_id' => Schema::TYPE_INTEGER,
            'comment' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_TIMESTAMP,
        ]);

        $this->createIndex('products_category_id', 'products', 'category_id');
        $this->addForeignKey('fk_products_category_id', 'products', 'category_id', 'categories', 'id');

        $this->createIndex('comments_product_id', 'comments', 'product_id');
        $this->addForeignKey('fk_comments_product_id', 'comments', 'product_id', 'products', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_products_category_id', 'products');
        $this->dropIndex('products_category_id', 'products');

        $this->dropForeignKey('fk_comments_product_id', 'comments');
        $this->dropIndex('comments_product_id', 'comments');

        $this->dropTable('categories');
        $this->dropTable('products');
        $this->dropTable('comments');
    }
}
