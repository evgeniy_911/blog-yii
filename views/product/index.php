<?php

/** @var Product $product */

use app\models\product\Product;

?>

<div class="nine columns">
    <h1><?= $product->title; ?></h1>
    <?= $product->created_at; ?>
    <div class="quote">
        <p>” It works for everyday people who love pottering about designing their dream home, and for professionals who need a robust program to create 2D and 3D interactive floor plans ”<br>
            <span class="blue">Sam Crothers, Houseplanology.com</span>
        </p>
    </div>
    <div class="content"><?= $product->content; ?></div>
    <hr>
    <a class="previous" href="/" aria-current="page">Назад</a>
    <h3> <?= $product->visits; ?></h3>
</div>

<?php foreach ($product->comments as $comment): ?>
    <?= $comment->comment; ?> <br>
    <?= $comment->product_id; ?> <hr>
<?php endforeach; ?>

<br>








