<div id="header" class="border-bottom mobile-hide"><!-- Start header -->
    <div class="row clearfix">
        <div class="twelve columns height-head">
            <div class="mobile-hide">
                <div id="logo">
                    <a href="/">S&E</a>
                </div>
            </div>
            <!-- Start site menu -->
            <nav id="top-menu">
                <ul>
                    <li id="menu-item-10" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2 current_page_item menu-item-10">
                        <a href="/contact" aria-current="page">Contact AS</a>
                    </li>
                    <li id="menu-item-10" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2 current_page_item menu-item-10">
                        <a href="/faq" aria-current="page">FAQ</a>
                    </li>
                    <li id="menu-item-10" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2 current_page_item menu-item-10">
                        <a href="/quiz" aria-current="page">Quiz</a>
                    </li>
                  </ul>
            </nav>
            <!-- End site menu -->
        </div>
    </div>
</div><!-- End header -->