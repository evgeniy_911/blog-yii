<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <?php include Yii::$app->basePath.'/views/layouts/head.php'; ?>

</head>
<body>

<?php include Yii::$app->basePath.'/views/layouts/header.php'; ?>
<?php include Yii::$app->basePath.'/views/layouts/header-mobile.php'; ?>

<div id="page-wrap">
    <div class="row clearfix"><!-- Start content -->
        <?php include Yii::$app->basePath.'/views/layouts/sidebar-menu.php'; ?>

        <?php $this->beginBody() ?>
            <?= $content ?>
        <?php $this->endBody() ?>
    </div>
</div>


<?php include Yii::$app->basePath.'/views/layouts/footer.php'; ?>
<?php include Yii::$app->basePath.'/views/layouts/footer-mobile.php'; ?>

<!-- material-scrolltop button -->
<button class="material-scrolltop" type="button"></button>

<!-- Initialize material-scrolltop (minimal) -->
<script>
    $('body').materialScrollTop();
</script>

</body>
</html>
<?php $this->endPage() ?>
