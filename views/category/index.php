<?php

/* @var $this yii\web\View */
/* @var $age string */
/* @var $products array */

$this->title = 'My Yii Application';

use app\widgets\GoogleCharts;

?>

<div class="nine columns">
    <form action="/category/<?= Yii::$app->request->get('category_id')?>" method="post">
        <select name="sort">
            <option value="alphabet_order">По алфавиту</option>
            <option value="attendance">По посещаемости</option>
            <option value="/">По новым</option>
        </select>

        <select name="limit">
            <option value="2">2</option>
            <option value="4">4</option>
            <option value="6">6</option>
        </select>

        <input type="text" name="search">
        <button>Найти</button>
    </form>

    Общее количество товаров под ходящих под ваш запрос:<?= count($products) ?>

    <?php foreach ($products as $product): ?>
        <div class="row clearfix bordergrey">
            <div class="six columns">
                <h3><?= $product->title; ?></h3>
                <p><?= $product->description; ?><br>
                    <br>
                    <a class="next" href="/product/<?= $product->url; ?>" aria-current="page">Подробнее</a>
                </p>
            </div>
            <?= $product->created_at; ?>
            <?= $product->category->title; ?>
            <div class="six columns">
                <figure class="c4-izmir c4-border-cc-3 c4-image-zoom-out c4-gradient-bottom-right" tabindex="0"
                        style="--primary-color: #7303c0; --secondary-color: #00B5CC;">
                    <img src="https://source.unsplash.com/FaPxZ88yZrw/400x300"  alt="Sample Image">
                    <figcaption>
                        <div class="c4-rotate-up-right c4-delay-200">
                            <h2>
                                Endless
                            </h2>
                        </div>
                        <div class="c4-rotate-down-left c4-delay-200">
                            <h2>
                                Possibilities
                            </h2>
                        </div>
                    </figcaption>
                </figure>
            </div>
        </div>
    <?php endforeach; ?>
</div>

























<?//= GoogleCharts::widget() ?>






