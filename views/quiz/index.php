<?php
/* @var int $balu */
/* @var  array $otvetu */
/* @var $vopross array */
/* @var $vopross array */
?>

<?php if (Yii::$app->request->isPost): ?>
<table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Вопрос</th>
                <th>Правильный ответ?</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($vopross as $vopros): ?>
                <tr>
                    <td><?= $vopros->id ?></td>
                    <td>
                        <a href="/quiz/view/<?= $vopros->id ?>"><?= $vopros->vopros; ?></a>
                    </td>
                    <td><?= $otvetu[$vopros->id] ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
</table>
<!--    --><?//= "Вы заработали $balu очков" ?>
<!--    <hr>-->
<!--    Ваши ответы --><?//= implode("<HR>", $otvetu) ?>
<?php else: ?>
    <form action="/quiz" method="post">
        <?php foreach ($vopross as $vopros): ?>
            <hr>
            <h5><?= $vopros->vopros; ?></h5>
            <select name="<?= $vopros->id ?>[]">
                <?php foreach ($vopros->otvets as $otvet): ?>
                    <option value="<?= $otvet->id ?>"><?= $otvet->otvet ?></option>
                    <?php
                        if ($otvet->verno == '1') {
                            $idVernogoOtveta = $otvet->id;
                        }
                    ?>
                <?php endforeach; ?>
            </select>

            <input hidden name="<?= $vopros->id ?>[]" type="text" value="<?= $idVernogoOtveta ?>">

        <?php endforeach; ?>
        <hr>
        <button>Пройти тест</button>
    </form>
<?php endif; ?>
