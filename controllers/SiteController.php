<?php

namespace app\controllers;

use app\models\category\Category;
use app\models\Fruts;
use app\models\Kart21;
use app\models\Lotery;
use app\models\PhpArraySum;
use app\models\PhpStandartFunction;
use app\models\product\Product;
use app\models\Table;
use app\models\Youtub;
use app\models\zzz\Zzz;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $criteria = ['created_at' => SORT_DESC];
        $limit = null;
        $search = '';

        if (Yii::$app->request->post('sort') == 'alphabet_order') {
            $criteria = ['title' => SORT_ASC];
        }

        if (Yii::$app->request->post('sort') == 'attendance') {
            $criteria = ['visits' => SORT_DESC];
        }

        if (!empty(Yii::$app->request->post('limit'))) {
            $limit = (int)Yii::$app->request->post('limit');
        }

        if (!empty(Yii::$app->request->post('search'))) {
            $search = Yii::$app->request->post('search');
        }

        $products = Product::find()
            ->where(['status' => Product::STATUS_ACTIVE])
            ->andWhere(['like', 'title', $search])
            ->orderBy($criteria)
            ->limit($limit)
            ->all();

        return $this->render('index', [
            'products' => $products,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $statistics = Zzz::find()->all();// [] foreach
        $statistic = Zzz::find()->where(['visits' => 1000])->one();// Zzz->

        return $this->render('about', [
            'statistics' => $statistics,
            'statistic' => $statistic,
        ]);
    }

    /**
     * @return string
     */
    public function actionFaq()
    {
        return $this->render('faq');
    }

    /**
     * @return string
     */
    public function actionContact()
    {
        return $this->render('contact');
    }
}
