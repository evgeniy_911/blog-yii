<?php

namespace app\controllers;


use app\models\otvet\Otvet;
use app\models\product\Product;
use app\models\vopros\Vopros;
use Yii;
use yii\web\Controller;

class QuizController extends Controller
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $vopross = Vopros::find()->all();
        $otvetss = Otvet::find()
            ->where(['verno' => 1])
            ->all();
        $balu = 0;
        $otvetu = [];

        if (Yii::$app->request->isPost) { // button
            $forms = Yii::$app->request->post();

            foreach ($forms as $idVoprosa => $form) {
                $otvet = '-';

                if ($form[0] == $form[1]) {
                    $otvet = '+';
                    $balu += 10;
                }

                $otvetu[$idVoprosa] = $otvet;
            }
        }

        return $this->render('index', [
            'vopross' => $vopross,
            'balu' => $balu,
            'otvetu' => $otvetu,
            'otvetss' => $otvetss,
        ]);

    }
    public function actionView()
    {
        $getId = Yii::$app->request->get();
        return $this->render('view', [
            'getId' => $getId,
        ]);
    }
}
