<?php

namespace app\controllers;

use app\models\product\Product;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ProductController extends Controller
{
    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $product = Product::find()->where([
            'url' => Yii::$app->request->get('url')
        ])->one();

        if (is_null($product)) {
            throw new NotFoundHttpException();
        }

        return $this->render('index', [
            'product' => $product,
        ]);
    }
}