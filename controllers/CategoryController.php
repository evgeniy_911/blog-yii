<?php

namespace app\controllers;

use app\models\product\Product;
use Yii;
use yii\web\Controller;

class CategoryController extends Controller
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $criteria = ['created_at' => SORT_DESC];
        $limit = null;
        $search = '';

        if (Yii::$app->request->post('sort') == 'alphabet_order') {
            $criteria = ['title' => SORT_ASC];
        }

        if (Yii::$app->request->post('sort') == 'attendance') {
            $criteria = ['visits' => SORT_DESC];
        }

        if (!empty(Yii::$app->request->post('limit'))) {
            $limit = (int)Yii::$app->request->post('limit');
        }

        if (!empty(Yii::$app->request->post('search'))) {
            $search = Yii::$app->request->post('search');
        }
        $products = Product::find()
            ->where([
                'category_id' => Yii::$app->request->get('category_id'),
                'status' => Product::STATUS_ACTIVE,
            ])
            ->andWhere(['like', 'title', $search])
            ->orderBy($criteria)
            ->limit($limit)
            ->all();

        return $this->render('index', [
            'products' => $products,
        ]);
    }
}
