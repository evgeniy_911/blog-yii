<?php

namespace app\models;

class Fruts
{
    private static $counOrangeReplace = 3;

    public static function replace($fruts)
    {
        foreach ($fruts as $k => $v) {
            if (self::$counOrangeReplace == 0) {
                break;
            }

            if ($v == 'orange') {
                $fruts[$k] = 'limon';
                self::$counOrangeReplace--;// 2
            }
        }

        return $fruts;
    }
}
