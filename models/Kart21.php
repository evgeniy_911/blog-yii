<?php

namespace app\models;

class Kart21
{
    private static $kartMap = [
        ['koroltref' => 4],
        ['damatref' => 3],
        ['valettref' => 2],
        ['tuztref' => 11],
        ['10tref' => 10],
        ['9tref' => 9],
        ['8tref' => 8],
        ['7tref' => 7],
        ['6tref' => 6],
        ['korolpik' => 4],
        ['damapik' => 3],
        ['valetpik' => 2],
        ['tuzpik' => 11],
        ['10pik' => 10],
        ['9pik' => 9],
        ['8pik' => 8],
        ['7pik' => 7],
        ['6pik' => 6],
        ['korolcherv' => 4],
        ['damacherv' => 3],
        ['valetcherv' => 2],
        ['tuzcherv' => 11],
        ['10cherv' => 10],
        ['9cherv' => 9],
        ['8cherv' => 8],
        ['7cherv' => 7],
        ['6cherv' => 6],
        ['korolbubn' => 4],
        ['damabubn' => 3],
        ['valetbubn' => 2],
        ['tuzbubn' => 11],
        ['10bubn' => 10],
        ['9bubn' => 9],
        ['8bubn' => 8],
        ['7bubn' => 7],
        ['6bubn' => 6],
    ];

    private $moiKarts = [];

    private $o4ki = 0;

    public function putKart($number)
    {
        $this->o4ki = $this->o4ki + reset($this->moiKarts[$number]);

        unset($this->moiKarts[$number]);

        var_dump($this->o4ki);
//        var_dump($this->moiKarts);
    }

    public function dobrat1Kart()
    {
        shuffle(self::$kartMap);

        $this->moiKarts = [self::$kartMap[0]];

        var_dump($this->moiKarts);
    }

    public function start()
    {
        shuffle(self::$kartMap);

        $this->moiKarts = [self::$kartMap[0], self::$kartMap[1]];

        var_dump($this->moiKarts);
    }
}
