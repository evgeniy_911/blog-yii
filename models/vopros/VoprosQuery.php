<?php

namespace app\models\vopros;

use app\models\vopros\Vopros;

/**
 * This is the ActiveQuery class for [[Vopros]].
 *
 * @see Vopros
 */
class VoprosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Vopros[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Vopros|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
