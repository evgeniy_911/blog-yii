<?php

namespace app\models\vopros;

use app\models\otvet\Otvet;
use app\models\otvet\OtvetQuery;
use Yii;

/**
 * This is the model class for table "vopros".
 *
 * @property int $id
 * @property string|null $vopros
 *
 * @property Otvet[] $otvets
 */
class Vopros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vopros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vopros'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vopros' => 'Vopros',
        ];
    }

    /**
     * Gets query for [[Otvets]].
     *
     * @return \yii\db\ActiveQuery|OtvetQuery
     */
    public function getOtvets()
    {
        return $this->hasMany(Otvet::className(), ['vopros_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return VoprosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VoprosQuery(get_called_class());
    }
}
