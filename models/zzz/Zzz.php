<?php

namespace app\models\zzz;

use Yii;

/**
 * This is the model class for table "zzz".
 *
 * @property int $id
 * @property string|null $category
 * @property int|null $visits
 */
class Zzz extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zzz';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['visits'], 'integer'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'visits' => 'Visits',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ZzzQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ZzzQuery(get_called_class());
    }
}
