<?php

namespace app\models\zzz;

/**
 * This is the ActiveQuery class for [[Zzz]].
 *
 * @see Zzz
 */
class ZzzQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Zzz[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Zzz|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
