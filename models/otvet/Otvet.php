<?php

namespace app\models\otvet;


use app\models\vopros\Vopros;
use app\models\vopros\VoprosQuery;
use Yii;

/**
 * This is the model class for table "otvet".
 *
 * @property int $id
 * @property int|null $vopros_id
 * @property string|null $otvet
 * @property string|null $kluch
 *
 * @property Vopros $vopros
 */
class Otvet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'otvet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vopros_id'], 'integer'],
            [['otvet', 'kluch'], 'string', 'max' => 255],
            [['vopros_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vopros::className(), 'targetAttribute' => ['vopros_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vopros_id' => 'Vopros ID',
            'otvet' => 'Otvet',
            'kluch' => 'Kluch',
        ];
    }

    /**
     * Gets query for [[Vopros]].
     *
     * @return \yii\db\ActiveQuery|VoprosQuery
     */
    public function getVopros()
    {
        return $this->hasOne(Vopros::className(), ['id' => 'vopros_id']);
    }

    /**
     * {@inheritdoc}
     * @return OtvetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OtvetQuery(get_called_class());
    }
}
