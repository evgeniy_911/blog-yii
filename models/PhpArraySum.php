<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Mixed_;

class PhpArraySum
{
    private static $monthMap = [
        1 => 'Январь', 2 => 'Февраль', 3 => 'Март', 4 => 'Апрель',
        5 => 'Май', 6 => 'Июнь',7 => 'Июль', 8 => 'Август',
        9 => 'Сентябрь', 10 => 'Октябрь',11 => 'Ноябрь', 12 => 'Декабрь'
    ];

    public static function arraySum(array $array): int
    {
        $num = 0;
        foreach ($array as $k => $v) {
            $num = $num + $v;
        }

        return $num;
    }

    public static function arraySearch(string $needle, array $array): int
    {
        foreach ($array as $k => $v) {
            if ($v == $needle) {
                return $k;
            }
        }
    }

    public static function arrayKeys(array $array): array
    {
        $result = [];
        foreach ($array as $k => $v) {
            $result[] = $k;
        }
        return $result;
    }

    public static function arrayKeyFirst(array $array)
    {
        foreach ($array as $k => $v) {
            return $k;
        }
    }

    public static function arrayKeyLast(array $array): string
    {
        $vsego = count($array); // vsego elementov

        foreach ($array as $k => $v) {
            $vsego--;
            if ($vsego == 0) {
                return $k;
            }
        }
//        $result = 0;
//        foreach ($array as $k => $v) {
//            $result = $k++;
//        }
//        return $result;
    }

    public static function arrayUnshift(array $array1, array $result): array
    {
        foreach ($array1 as $k => $v) {
            $result[] = $v;
        }

        return $result;
    }

    public static function arrayEnd(array $array): string
    {
        $vsego = count($array); // vsego elementov

        foreach ($array as $k => $v) {
            $vsego--;
            if ($vsego == 0) {
                return $v;
            }
        };
    }

    public static function arrayChangeKeyCase(array $array): array
    {
        $nov = [];
        foreach ($array as $k => $v) {
            $nov[] = mb_strtoupper($k);
        }

        return $nov;
    }

    public static function arrayValue(array $array): array
    {
        $result = [];
        foreach ($array as $k => $v) {
            $result[] = $v;
        }

        return $result;
    }

    public static function arrayUnique(array $array): array
    {
        $result = [];
        foreach ($array as $k => $v) {
            if (!in_array($v, $result)) {
                $result[$k] = $v;
            }
        }

        return $result;
    }

    public static function arrayCollumn(array $array, string $fieldName)
    {
        $result = [];

        foreach ($array as $k => $v) {
            $result[] = $v[$fieldName];
        }

        return $result;
    }

    public static function arrayRange(int $start, int $finish): array
    {
        $result = [];

        for ($i = $start; $i <= $finish; $i++) {
            $result[] = $i;
        }

        return $result;
    }

    public static function arrayStrlen(string $stroka): int
    {
        return count(str_split($stroka));
    }

    public static function arrayImplode(string $tire, array $array): string
    {
        $result = '';
        $vsego = count($array);
        foreach ($array as $k => $v) {
            $vsego--;
            if ($vsego == 0)  {
                $result .= $v;
            } else {
                $result .= $v . $tire;
            }
        }

        return $result;
    }

    public static function getMonthByNumber(int $number): string
    {
        return self::$monthMap[$number];
    }

    public static function getNumberByMonth(string $month): int
    {
        return array_flip(self::$monthMap)[$month];
    }

    public static function sprintf(string $stroka, int $val): string
    {
         $result = str_replace("%d", $val, $stroka);
         return $result;
    }

}

$users = [
    [
        'id' => 1,
        'firstname' => 'Sergey',
        'lastname' => 'Ischuk',
        'age' => 32,
        'ban' => true,
        'salary' => [100, 200, 220],
    ],
    [
        'id' => 2,
        'firstname' => 'Zhenya',
        'lastname' => 'Chernuy',
        'age' => 35,
        'ban' => true,
        'salary' => [100, 200],
    ],
    [
        'id' => 3,
        'firstname' => 'Zhenya2',
        'lastname' => 'Chernuy',
        'age' => 15,
        'ban' => false,
        'salary' => [220],
    ],
];

$value = Yii::$app->request->get('value');

//$salary = array_sum ( array ($users['salary']));
//var_dump($users['salary']); die();
?>

<a href="?value=all">Все юзеры</a> <br>
<a href="?value=noage">Все юзеры только без возраста</a> <br>
<a href="?value=otido">Все юзеры возраст от 16 до 33 вывести</a> <br>
<a href="?value=salary">Зарплата</a> <br>
<a href="?value=age">Все юзеры возраст или 35 или 32 вывести</a> <br>

<table class="table table-bordered">
    <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Age</th>
        <th>Salary</th>
    </tr>

    <?php foreach ($users as $user): ?>
        <?php if($value === 'all' ): ?>
            <tr>
                <td><?= $user['firstname']; ?></td>
                <td><?= $user['lastname']; ?></td>
                <td><?= $user['age']; ?></td>
            </tr>
        <?php endif; ?>
        <?php if($value === 'noage'): ?>
            <tr>
                <td><?= $user['firstname']; ?></td>
                <td><?= $user['lastname']; ?></td>
                <td></td>
            </tr>
        <?php endif; ?>
        <?php if($value === 'otido' && $user['age'] > 16 && $user['age'] < 33): ?>
            <tr>
                <td><?= $user['firstname']; ?></td>
                <td><?= $user['lastname']; ?></td>
                <td><?= $user['age']; ?></td>
            </tr>
        <?php endif; ?>
        <?php if($value === 'salary') :?>
            <tr>
                <td><?= $user['firstname']; ?></td>
                <td><?= $user['lastname']; ?></td>
                <td><?= $user['age']; ?></td>
                <td><?= array_sum($user['salary']); ?></td>
            </tr>
        <?php endif; ?>
        <?php if($value === 'age' && $user['age'] != 32 || $user['age'] != 35): ?>
            <tr>
                <td><?= $user['firstname']; ?></td>
                <td><?= $user['lastname']; ?></td>
                <td><?= $user['age']; ?></td>
            </tr>
        <?php endif; ?>
    <?php endforeach; ?>
</table>
