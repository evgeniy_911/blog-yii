<?php

namespace app\models;

class Lotery
{
    private static $winNumber = 7;

    public static function play(int $myNumber): bool
    {
        return $myNumber === self::$winNumber;
    }
}
