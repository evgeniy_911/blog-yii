<?php

namespace app\models;

class Youtub
{
    private static $url = 'https://www.youtube.com/';

    public static function generateURL(array $urls): array
    {
        foreach ($urls as $k => $v) {
            $urls[$k] = self::$url . $v;
        }

        return $urls;
    }
}