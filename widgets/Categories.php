<?php

namespace app\widgets;

use app\models\category\Category;
use yii\base\Widget;

class Categories extends Widget
{
    public function run()
    {
        $categories = Category::find()->all();

        return $this->render('categories', [
            'categories' => $categories,
        ]);
    }
}
