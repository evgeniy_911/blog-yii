<?php

namespace app\widgets;

use app\models\zzz\Zzz;
use yii\base\Widget;

class GoogleCharts extends Widget
{
    public function run()
    {
        $statistics = Zzz::find()->all();

        return $this->render('google_charts', [
            'statistics' => $statistics,
        ]);
    }
}
