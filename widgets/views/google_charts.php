<?php
/** @var array $statistics */

$result = '[';
foreach ($statistics as $statistic) {
    $result .= "['" . $statistic->category . "', " . $statistic->visits . "],";
}
$result .= ']';

$result2 = "[['Element', 'Visits'],";
foreach ($statistics as $statistic) {
    $result2 .= "['" . $statistic->category . "', " . $statistic->visits . "],";
}
$result2 .= ']';

?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item active">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Круг</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Диаграмма</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade active in" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div id="chart_div"></div>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div id="columnchart_values"></div>
    </div>
</div>



<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows(<?php echo $result; ?>);

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data);
    }
</script>

<script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable(
            <?php echo $result2; ?>
        );

        var view = new google.visualization.DataView(data);
        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
        chart.draw(view);
    }
</script>