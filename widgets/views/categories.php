<?php /** @var array $categories */

use yii\web\ErrorAction; ?>

<?php if ($categories && !Yii::$app->controller->action instanceof ErrorAction): ?>
    <div class="three columns mobile-hide">
        <div id="sidebar"><!-- Start sidebar -->
            <div class="widget">
                <ul id="sidebar-menu">
                    <?php foreach ($categories as $category): ?>

                        <li class="animated bounceInLeft current_page_item">
                            <a href="/category/<?= $category->id; ?>" aria-current="page">
                                <?= $category->title; ?>
                            </a>
                        </li>

                    <?php endforeach; ?>
                </ul>
            </div>
        </div><!-- End sidebar -->
    </div>
<?php endif; ?>